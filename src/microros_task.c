#include "microros.h"

#define RCCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){LPERROR("Failed status %d. Aborting.\n", (int)temp_rc); shutdown_req=1; vTaskDelete(NULL); }}
#define RCSOFTCHECK(fn) { rcl_ret_t temp_rc = fn; if((temp_rc != RCL_RET_OK)){xil_printf("Failed status on line %d: %d. Continuing.\n",__LINE__,(int)temp_rc);}}

rcl_publisher_t ping_publisher;
rcl_publisher_t pong_publisher;
rcl_subscription_t ping_subscriber;
rcl_subscription_t pong_subscriber;

std_msgs__msg__Header incoming_ping;
std_msgs__msg__Header outcoming_ping;
std_msgs__msg__Header incoming_pong;

int device_id;
int seq_no;
int pong_count;


void ping_timer_callback(rcl_timer_t * timer, int64_t last_call_time)
{
	RCLC_UNUSED(last_call_time);

if (timer != NULL) {

		seq_no = rand();
		sprintf(outcoming_ping.frame_id.data, "%d_%d", seq_no, device_id);
		outcoming_ping.frame_id.size = strlen(outcoming_ping.frame_id.data);

		// Fill the message timestamp
		struct timespec ts;
		clock_gettime(CLOCK_REALTIME, &ts);
		outcoming_ping.stamp.sec = ts.tv_sec;
		outcoming_ping.stamp.nanosec = ts.tv_nsec;

		// Reset the pong count and publish the ping message
		pong_count = 0;
		if (rcl_publish(&ping_publisher, (const void*)&outcoming_ping, NULL) != RCL_RET_OK) LPERROR("Fail to publish a ping\r\n");
		PROD_LPRINTF("Ping send seq %s\r\n", outcoming_ping.frame_id.data);
	}
}


void ping_subscription_callback(const void * msgin)
{
	const std_msgs__msg__Header * msg = (const std_msgs__msg__Header *)msgin;

	// Dont pong my own pings
	if(strcmp(outcoming_ping.frame_id.data, msg->frame_id.data) != 0){
		PROD_LPRINTF("Ping received with seq %s. Answering.\r\n", msg->frame_id.data);
		if (rcl_publish(&pong_publisher, (const void*)msg, NULL) != RCL_RET_OK) LPERROR("Fail to publish a pong\r\n");
	}
}


void pong_subscription_callback(const void * msgin)
{
	const std_msgs__msg__Header * msg = (const std_msgs__msg__Header *)msgin;

	if(strcmp(outcoming_ping.frame_id.data, msg->frame_id.data) == 0) {
		pong_count++;
		PROD_LPRINTF("Pong for seq %s (%d)\r\n", msg->frame_id.data, pong_count);
	}
}

/*-----------------------------------------------------------------------------*
 *  micro-ROS Task
 *-----------------------------------------------------------------------------*/
void microros_task_main(void *argument)
{
	LPRINTF("╔═══════════════════════════════════════════════════════╗\r\n");
	LPRINTF("║  Micro-ROS task created                               ║\r\n");
	LPRINTF("╚═══════════════════════════════════════════════════════╝\r\n");

	/* allocators to enable micro-ROS */
	rcl_allocator_t allocator = rcl_get_default_allocator();
	rclc_support_t support;

	rcl_allocator_t freeRTOS_allocator = rcutils_get_zero_initialized_allocator();
	freeRTOS_allocator.allocate = __freertos_allocate;
	freeRTOS_allocator.deallocate = __freertos_deallocate;
	freeRTOS_allocator.reallocate = __freertos_reallocate;
	freeRTOS_allocator.zero_allocate = __freertos_zero_allocate;

	if (!rcutils_set_default_allocator(&freeRTOS_allocator)) {
		LPERROR("Error on default allocators (line %d)\r\n",__LINE__);
	}

	#ifdef RMW_UXRCE_TRANSPORT_CUSTOM
	/* Defines the structure of the rpmsg system parametrs */
	rpmsg_param *rpmsg_param_set = malloc(sizeof(rpmsg_param));
	rmw_uros_set_custom_transport(
			true,
			&rpmsg_param_set,
			my_custom_transport_open,
			my_custom_transport_close,
			my_custom_transport_write,
			my_custom_transport_read
		);
	#endif /* RMW_UXRCE_TRANSPORT_CUSTOM */

	// create init_options
	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
	LPRINTF("│ Checking the rclc_support_init function                                   │\r\n");
	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯\r\n");
	RCCHECK(rclc_support_init(&support, 0, NULL, &freeRTOS_allocator));

	// create node
	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
	LPRINTF("│ Checking the rclc_node_init_default function                              │\r\n");
	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯\r\n");
	rcl_node_t node;
	RCCHECK(rclc_node_init_default(&node, "pingpong_node", "", &support));

	// Setup the ping as a topic
	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
	LPRINTF("│ Checking the rclc_publisher_init_default function                         │\r\n");
	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯\r\n");
	RCCHECK(rclc_publisher_init_best_effort(&ping_publisher, &node,
		ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Header), "/microROS/ping"));

	// Create a best effort pong publisher
	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
	LPRINTF("│ Checking the rclc_publisher_init_best_effort function                     │\r\n");
	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯\r\n");
	RCCHECK(rclc_publisher_init_best_effort(&pong_publisher, &node,
		ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Header), "/microROS/pong"));

	// Create a best effort ping subscriber
	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
	LPRINTF("│ Checking the rclc_subscription_init_best_effort function                  │\r\n");
	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯\r\n");
	RCCHECK(rclc_subscription_init_best_effort(&ping_subscriber, &node,
		ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Header), "/microROS/ping"));

	// Create a best effort  pong subscriber
	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
	LPRINTF("│ Checking the rclc_subscription_init_best_effort function                  │\r\n");
	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯\r\n");
	RCCHECK(rclc_subscription_init_best_effort(&pong_subscriber, &node,
		ROSIDL_GET_MSG_TYPE_SUPPORT(std_msgs, msg, Header), "/microROS/pong"));


	// Create a 1 second ping timer,
//	LPRINTF("╭───────────────────────────────────────────────────────────────────────────╮\r\n");
//	LPRINTF("│ Checking the rclc_timer_init_default function                             │\r\n");
//	LPRINTF("╰───────────────────────────────────────────────────────────────────────────╯ \r\n");
//	rcl_timer_t timer;
//	RCCHECK(rclc_timer_init_default(&timer, &support, RCL_MS_TO_NS(1000), ping_timer_callback));


	// Create executor
	rclc_executor_t executor;
	RCCHECK(rclc_executor_init(&executor, &support.context, 3, &allocator));
	//RCCHECK(rclc_executor_add_timer(&executor, &timer));
	RCCHECK(rclc_executor_add_subscription(&executor, &ping_subscriber, &incoming_ping,
		&ping_subscription_callback, ON_NEW_DATA));
	RCCHECK(rclc_executor_add_subscription(&executor, &pong_subscriber, &incoming_pong,
		&pong_subscription_callback, ON_NEW_DATA));

	// Create and allocate the pingpong messages
	char outcoming_ping_buffer[STRING_BUFFER_LEN];
	outcoming_ping.frame_id.data = outcoming_ping_buffer;
	outcoming_ping.frame_id.capacity = STRING_BUFFER_LEN;

	char incoming_ping_buffer[STRING_BUFFER_LEN];
	incoming_ping.frame_id.data = incoming_ping_buffer;
	incoming_ping.frame_id.capacity = STRING_BUFFER_LEN;

	char incoming_pong_buffer[STRING_BUFFER_LEN];
	incoming_pong.frame_id.data = incoming_pong_buffer;
	incoming_pong.frame_id.capacity = STRING_BUFFER_LEN;

	device_id = rand();

	/* to test the exit function, I can try to avoid spin completely */
	while(1){
		rclc_executor_spin_some(&executor, RCL_MS_TO_NS(10));
//		if (shutdown_req) {
//			RCCHECK(rcl_node_fini(&node));
//		}
		usleep(1000);

	}

	// Free resources
	RCCHECK(rcl_publisher_fini(&ping_publisher, &node));
	RCCHECK(rcl_publisher_fini(&pong_publisher, &node));
	RCCHECK(rcl_subscription_fini(&ping_subscriber, &node));
	RCCHECK(rcl_subscription_fini(&pong_subscriber, &node));
	RCCHECK(rcl_node_fini(&node));

	free(rpmsg_param_set);

}


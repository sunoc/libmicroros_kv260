#include "microros.h"

/* Initialize the shutdown flag as a global variable */
int shutdown_req = 0;

/* Flush flag for the cb function. By default, we don't flush. */
int fresh_agent_packets_flag = 1;

/* keeping a single rpmsg end point for simplicity */
struct rpmsg_endpoint ept;

/* external variables and definitions */
//extern SemaphoreHandle_t xSemaphore_rpmsg_task;
extern SemaphoreHandle_t xSemaphore_uros_task;

//extern void wakeuptaskISP(SemaphoreHandle_t semaphore);
extern void blockingtask(SemaphoreHandle_t semaphore);

/* RPMsg read buffer */
extern StreamBufferHandle_t read_buffer_h;

/* Dummy reply function to test the Agent replies */
void rpmsg_testbench_task_main(void *received_message);


/*-----------------------------------------------------------------------------*
 *  RPMSG end point callback
 *-----------------------------------------------------------------------------*/
static int rpmsg_endpoint_cb(struct rpmsg_endpoint *ept, void *data, size_t len,
			     uint32_t src, void *priv)
{
	LPRINTF("Potential context switch: RPMsg callback function called.\r\n");
	LPRINTF("═════════════════════════════════════════════════════════\r\n");

	(void)priv;
	(void)src;
	size_t xBytesSent;
	const TickType_t xBlockTime = pdMS_TO_TICKS( 100 );

	int hello_flg = 1;

	/* On reception of a shutdown we signal the application to terminate */
	if ((*(unsigned int *)data) == SHUTDOWN_MSG) {
		LPRINTF("shutdown message is received.\r\n");
		shutdown_req = 1;
		return RPMSG_SUCCESS;
	}

	LPRINTF("Got some data of length %d:\r\n", (int)len);


	for (int i = 0; i<(int)len; i++){
		/* Test of the "hello" message from agent
		 * We check here is the whole message is being made of
		 * "42" values. */
		hello_flg = hello_flg && ( 42 == ((uint8_t *)data)[i] );

		LPRINTF("0x%x\r\n", ((uint8_t *)data)[i]);
	}

	/* if hello message is received, exiting without
	 * updating the read buffer. */
	if ( hello_flg ) {
		LPRINTF("The whole ``hello`` message was received. Exiting callback.\r\n");
		return RPMSG_SUCCESS;

	/* If something else is received,
	 * we are trying to put it in the read buffer,
	 * so the custom transport read function can
	 * access these data.
	 * */
	} else {

		// dummy testbench function
		// rpmsg_testbench_task_main(data);

		/* We are receiving a fresh set of packet from the agent ! Flushing the read_buffer_h */
		if ( 0 == fresh_agent_packets_flag ) {
			fresh_agent_packets_flag = 1;
			LPRINTF("===> Flushing the read_buffer_h.\r\n");
			if ( xMessageBufferReset(read_buffer_h) != pdTRUE )
				LPERROR("Unable to empty the read_buffer_h after read.\r\n");
		}

		/* Write data to the read buffer as long as it is not full. */
		if (pdFALSE == xStreamBufferIsFull(read_buffer_h)){
			LPRINTF("Putting the received data of length %d, first byte being 0x%x in the read_buffer_h: 0x%x.\r\n",
					len,
					((uint8_t *)data)[0],
					read_buffer_h);
			xBytesSent = xStreamBufferSend( read_buffer_h,
											data,
											len,
											xBlockTime );
			if ( len != xBytesSent ){
				LPERROR("Failed to put %d bytes in the read buffer. Only %d.\r\n", len, xBytesSent);
				return RPMSG_ERROR_BASE;
			} else {
				LPRINTF("Data put in read_buffer_h. Returning success.\r\n");
				return RPMSG_SUCCESS;
			}
		} else {
			LPERROR("Ayayay, the read buffer is full.\r\n");
			return RPMSG_ERROR_BASE;
		}
	}
}

/*-----------------------------------------------------------------------------*
 *  RPMSG unbind callback
 *-----------------------------------------------------------------------------*/
static void rpmsg_service_unbind(struct rpmsg_endpoint *ept)
{
	(void)ept;
	ML_INFO("unexpected Remote endpoint destroy\r\n");
	shutdown_req = 1;
}

/*-----------------------------------------------------------------------------*
 *  RPMsg Task
 *-----------------------------------------------------------------------------*/
void rpmsg_task_main(void *unused_arg)
{
	LPRINTF("╔═══════════════════════════════════════════════════════╗\r\n");
	LPRINTF("║  RPMsg task created                                   ║\r\n");
	LPRINTF("╚═══════════════════════════════════════════════════════╝\r\n");

	void *platform;
	struct rpmsg_device *rpdev;
	int ret;
	char ept_name[EPT_NAME_LEN] = RPMSG_SERVICE_NAME;


	/* can't use ML_INFO, metal_log setup is in init_system */
	LPRINTF("openamp library version: %s\r\n", openamp_version());
	LPRINTF("Major: %d, \r\n", openamp_version_major());
	LPRINTF("Minor: %d, \r\n", openamp_version_minor());
	LPRINTF("Patch: %d)\r\n", openamp_version_patch());

	LPRINTF("libmetal lib version: %s \r\n", metal_ver());
	LPRINTF("Major: %d, \r\n", metal_ver_major());
	LPRINTF("Minor: %d, \r\n", metal_ver_minor());
	LPRINTF("Patch: %d)\r\n", metal_ver_patch());

	LPRINTF("Starting application...\r\n");

	/* Initialize platform */
	if (platform_init(0, NULL, &platform)) {
		LPERROR("Failed to initialize platform. It sucks.\r\n");
	} else {
		rpdev = platform_create_rpmsg_vdev(platform, 0,
						   VIRTIO_DEV_DEVICE,
						   NULL, NULL);
		if (!rpdev){
			LPERROR("Failed to create rpmsg virtual IO device.\r\n");
		} else {
			/* initialize the length of the data to be sent and received*/
//			shared_len = 0;

			/* Initialize RPMSG framework */
			ret = rpmsg_create_ept(&ept, rpdev, ept_name,
						   RPMSG_ADDR_ANY, RPMSG_ADDR_ANY,
						   rpmsg_endpoint_cb,
						   rpmsg_service_unbind);
			if (ret) {
				LPERROR("Failed to create end point.\r\n");
				shutdown_req = 1;
			}

			LPRINTF("Successfully created rpmsg end point %s.\r\n", ept_name);


			LPRINTF("Waiting here until the IRQ is triggered once.\r\n");
			platform_poll(platform);

			/* The init phases stops at this poll
			 * utile the agent side is ran and the "hello" data packet
			 * is received by the callback function. */
			LPRINTF("Poll once more, after the IRQ was set properly.\r\n");
			platform_poll(platform);

			LPRINTF("Unlocking uROS task and entering rpmsg infinite polling.\r\n");
			wakeuptask(xSemaphore_uros_task);
			for ( ; ; ) {

				/* SHUTDOWN BLOC
				 * -------------
				 * Test for timeout or shutdown requests
				 * If something is received, we delete everything
				 * cleanly
				 * */
				if (1 == shutdown_req) {
					LPRINTF("we got a shutdown request, exit\r\n");
					for (int i = 0; i < ECHO_NUM_EPTS; i++) {
						/*
						 * Ensure that kernel does not destroy end point twice
						 * by disabling NS announcement. Kernel will handle it.
						 */
						(&ept)->rdev->support_ns = 0;
						LPRINTF("Destroying the end point...\r\n");
						rpmsg_destroy_ept(&ept);
					}

					LPRINTF("Releasing the virtual device...\r\n");
					platform_release_rpmsg_vdev(rpdev, platform);

					LPRINTF("Stopping application...\r\n");
					platform_cleanup(platform);

					/* Terminate this task */
					LPRINTF("Shutdown message receive. Deleting rpmsg general task.\r\n");
					vTaskDelete(NULL);
					LPERROR("If this message is viewed, the task was not killed properly.\r\n");
					break;
				}

				/* Else, polling */
				platform_poll(platform);

			}
		}
	}
}

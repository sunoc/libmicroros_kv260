/*
 * This file with headers and includes for the
 * Micro-ROS Ping Pong application.
 */

#ifndef MICROROS_H_
#define MICROROS_H_


#define ENABLE_POS 1
#define DEBUG_MODE 1

#include <rcl/rcl.h>
#include <rcl/error_handling.h>
#include <errno.h>
//#include <rcl/allocator.h>
#include <rcl/publisher.h>

#include <rmw_microros/rmw_microros.h>

#include <rclc/rclc.h>
#include <rclc/executor.h>

#include <std_msgs/msg/header.h>

#include <stdio.h>
#include "xil_printf.h"
#include <openamp/open_amp.h>
#include <openamp/version.h>
//#include <metal/alloc.h>
#include <metal/log.h>
#include <metal/version.h>

#include <rmw_microxrcedds_c/config.h>


#include <unistd.h>
#include <rmw/time.h>


#include <xtime_l.h>
#include <sys/types.h>
#include <sys/_timeval.h>
#include <sys/_timespec.h>

#include <uxr/client/client.h>
#include <ucdr/microcdr.h>

//#include "rpmsg-echo.h"
#include "platform_info.h"
//#include "memory_manager.h"

#include "xparameters.h"
#include "xuartps_hw.h"
#include "xuartps.h"

#include "message_buffer.h"


#define SHUTDOWN_MSG	0xEF56A55A
#define RPMSG_SERVICE_NAME         "rpmsg-openamp-demo-channel"

extern int shutdown_req;

#define EPT_NAME_LEN 32

#ifndef ECHO_NUM_EPTS
#define ECHO_NUM_EPTS 1
#endif /* !ECHO_NUM_EPTS */

#define CLOCK_REALTIME		((unsigned long long) 1)

/* The buffer for the custom */
#define xBufferSizeBytes 600
#define xTriggerLevel 1

/* Message length for the pingpong messages */
#define STRING_BUFFER_LEN xBufferSizeBytes


#define MICROSECONDS_PER_SECOND    ( 1000000LL )                                   /**< Microseconds per second. */
#define NANOSECONDS_PER_SECOND     ( 1000000000LL )                                /**< Nanoseconds per second. */
#define NANOSECONDS_PER_TICK       ( NANOSECONDS_PER_SECOND / configTICK_RATE_HZ ) /**< Nanoseconds per FreeRTOS tick. */

/* print and error messages utils */
//#define DEBUG

#ifdef DEBUG
#define LPRINTF(fmt, ...) xil_printf("%s():%u " fmt, __func__, __LINE__, ##__VA_ARGS__)
#define LPERROR(fmt, ...) LPRINTF("ERROR: " fmt, ##__VA_ARGS__)
#else
#define LPRINTF(fmt, ...) do {} while (0)
#define LPERROR(fmt, ...) do {} while (0)
#endif

/* Prints used anyway, and that should not slow the firmware significatly */
#define PROD_LPRINTF(fmt, ...) xil_printf("%s():%u " fmt, __func__, __LINE__, ##__VA_ARGS__)

typedef struct rpmsg_param{
	void *platform;
	struct rpmsg_device *rpdev;
	struct rpmsg_endpoint *ept;
} rpmsg_param;

int clock_gettime( clockid_t clock_id,
                   struct timespec * tp );

/* Memory management headers */
void *pvPortMalloc(size_t xWantedSize);
void vPortFree(void *pv);
size_t getBlockSize(void *pv);
void *pvPortRealloc(void *pv, size_t xWantedSize);
void *pvPortCalloc(size_t num, size_t xWantedSize);

/* Allocation function wrappers */
void * __freertos_allocate(size_t size, void * state);
void __freertos_deallocate(void * pointer, void * state);
void * __freertos_reallocate(void * pointer, size_t size, void * state);
void * __freertos_zero_allocate(size_t number_of_elements, size_t size_of_element, void * state);

/* Semaphore wraper functions */
void blockingtask_log(SemaphoreHandle_t semaphore);
void blockingtask(SemaphoreHandle_t semaphore);
void wakeuptaskISR(SemaphoreHandle_t semaphore);
void wakeuptask(SemaphoreHandle_t semaphore);

/* Custom transport functions */
bool my_custom_transport_open(struct uxrCustomTransport * transport);
bool my_custom_transport_close(struct uxrCustomTransport * transport);
size_t my_custom_transport_write(struct uxrCustomTransport* transport, const uint8_t * buf, size_t len, uint8_t * err);
size_t my_custom_transport_read(struct uxrCustomTransport* transport, uint8_t* buf, size_t len, int timeout, uint8_t* err);

/* Early init custom transport check function */
rcl_ret_t early_check();

/* Main call for the micro-ROS function */
void microros_task_main(void *argument);

/* Main call for the rpmsg function */
void rpmsg_task_main(void *unused_arg);



#endif /* MICROROS_H_ */

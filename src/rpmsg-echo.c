/*
 * SPDX-License-Identifier: GPL-2.0-only
 */

/*
 * This is a sample demonstration application that showcases usage of rpmsg
 * as a transport layer for DDS packages, sending data back and forth
 * a micro-ROS instance deployed here as a library and a separated task.
 * This application is meant to run on the remote CPU running baremetal code.
 * This application echoes back data that was sent to it by the host core
 * after the extensive rclc init procedure required for DDS.
 */
/*
In addition this application supports multiple endpoints. If the
macro ECHO_NUM_EPTS is set to a number >1, then this application
echoes back via one of N endpoints that correspond 1:1 with endpoints
on the host.

This firmware is expected to be loaded via Xilinx R5 Remoteproc kernel driver.

Usage on Linux to interact with this is as follows:

1. Load Firmware on RPU:
echo rpmsg_pingpong_microros_lib.elf > /sys/class/remoteproc/remoteproc0/firmware
echo start > /sys/class/remoteproc/remoteproc0/state

2. Start the Firmware
echo start > /sys/class/remoteproc/remoteproc0/state
*/

#include "microros.h"

#if ECHO_NUM_EPTS < 1 || ECHO_NUM_EPTS > 5
#error "ECHO_NUM_EPTS should be from 1 to 5"
#endif /* ECHO_NUM_EPTS < 1 || ECHO_NUM_EPTS > 5 */

//static struct rpmsg_endpoint lept[ECHO_NUM_EPTS];
static TaskHandle_t comm_task;
SemaphoreHandle_t xSemaphore_rpmsg_task = NULL;
SemaphoreHandle_t xSemaphore_uros_task = NULL;

/*-----------------------------------------------------------------------------*
 *  Application entry point
 *-----------------------------------------------------------------------------*/
int main(int ac, char **av)
{
	BaseType_t stat;

	/* Create the counting semaphore for the RPMsg lock system */
	xSemaphore_rpmsg_task   = xSemaphoreCreateCounting(32, 0);
	if( xSemaphore_rpmsg_task == NULL )
		LPERROR("Not enough memory on the heap for the RPMsg semaphore.\r\n");

	/* Create the binary semaphore for the micro-ROS lock system */
	xSemaphore_uros_task    = xSemaphoreCreateBinary();
	if( xSemaphore_uros_task == NULL)
		LPERROR("Not enough memory on the heap for the microros semaphore.\r\n");


	/* Create the tasks */
	stat = xTaskCreate(microros_task_main, ( const char * ) "micro-ROS_task",
				1024, NULL, 3, &comm_task);
	if (stat != pdPASS) {
		LPERROR("cannot create the micro-ROS task\r\n");
	} else {
		/* Start running FreeRTOS tasks  */
		vTaskStartScheduler();
	}

	/* Will not get here, unless a call is made to vTaskEndScheduler() */
	for ( ; ; ) ;

	/* suppress compilation warnings*/
	return 0;
}

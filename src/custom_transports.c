#include "microros.h"

#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <stdbool.h>
#include <metal/irq.h>

static TaskHandle_t rpmsg_task;

extern struct rpmsg_endpoint ept;

/* RPMsg read buffer */
StreamBufferHandle_t read_buffer_h;

extern SemaphoreHandle_t xSemaphore_uros_task;

#define UART_DEVICE_ID       1
#define UART1_BASEADDRESS 0xFF010000

/* A rpmsg flag asking to kill all tasks */
extern int shutdown_req;

extern int fresh_agent_packets_flag;


extern SemaphoreHandle_t xSemaphore_rpmsg_task;
extern void wakeuptask(SemaphoreHandle_t semaphore);


/* This is a ping pong echo test payload structure */
struct _payload {
  unsigned long num;
  unsigned long size;
  char data[];
};

struct rpmsg_device *rpdev;

typedef struct {
	size_t head;
    size_t tail;
    size_t size;
    char* data;
} queue_t;

/* File-wide available pointers for the buffers*/
queue_t read_Mbuffer;
queue_t write_Mbuffer;


#ifdef RMW_UXRCE_TRANSPORT_CUSTOM


/****************************************************************************/
/**
*
* This function should open and init the custom transport.
*
* @param	transport is a uxrCustomTransport structure
*
* @return	a boolean indicating if the opening was successful.
*
* @note		None.
*
*****************************************************************************/
bool my_custom_transport_open(struct uxrCustomTransport * transport)
{
	//LPRINTF("Opening custom transport channel...\r\n");

	BaseType_t stat_rpmsp;
	TaskStatus_t xTaskStatus;

	LPRINTF("Setting up the data buffer handle\r\n");
	read_buffer_h  = xStreamBufferCreate( xBufferSizeBytes, xTriggerLevel );
	if ( read_buffer_h == NULL ){
		LPERROR("Message buffers creation failed. Exiting.\r\n");
		shutdown_req = 1;
		return false;
	}

	LPRINTF("Create the RPMsg task\r\n");
	stat_rpmsp = xTaskCreate(rpmsg_task_main, ( const char * ) "RPMsg_task",
							 1024, NULL, 4, &rpmsg_task);

	LPRINTF("micro-ROS Task blocked. Waiting to confirm ept opening.\r\n");
	LPRINTF("═════════════════════════════════════════════\r\n");
	blockingtask(xSemaphore_uros_task);

	vTaskGetInfo(rpmsg_task, &xTaskStatus, pdFALSE, eInvalid);


	if (stat_rpmsp != pdPASS) {
		LPERROR("cannot create RPMsg task.\r\n");
		return false;

	} else if (xTaskStatus.eCurrentState == eDeleted){
		LPERROR("Task was killed in the mean time. Task opening failed.\r\n");
		return false;
	} else {
		LPRINTF("RPMsg communication channel successfully opened.\r\n");
		return true;
	}
}

/****************************************************************************/
/**
*
* This function should close the custom transport.
*
* @param	transport is a uxrCustomTransport structure
*
* @return	a boolean indicating if closing was successful.
*
* @note		None.
*
*****************************************************************************/
bool my_custom_transport_close(struct uxrCustomTransport * transport)
{
	LPRINTF("Sending signal to close the custom transport channel...\r\n");
	shutdown_req = 1;
	wakeuptask(xSemaphore_rpmsg_task);



	LPRINTF("Successfully close the rpmsg transport. Returning true.\r\n");
	return true;
}

/****************************************************************************/
/**
*
* This function should write data to the custom transport.
*
* @param transport is a uxrCustomTransport structure
* @param buf is a buffer for the data to be sent
* @param len is the number of bytes from the buffer to be sent
* @param err is unused as for now
*
* @return	the number of Bytes written.
*
* @note		None.
*
*****************************************************************************/
size_t my_custom_transport_write(struct uxrCustomTransport* transport,
								 const uint8_t * buf, size_t len, uint8_t * err)
{
	int ret;
	//unsigned int flags;

	//LPRINTF("Disabling IRQ while we try and send data...\r\n");
	//flags = metal_irq_save_disable();

	//LPRINTF("Try and send buf with len %d:\r\n", len);
//	for ( int i = 0; i<len; i++ ){
//		LPRINTF("index %d, data %c (0x%x)\r\n", i, buf[i], buf[i]);
//	}

	ret = rpmsg_send(&ept, buf, len);
	if ( 0 > ret ){
		LPERROR("rpmsg_send failed with value %d\r\n", ret);
		return ret;
	} else if ( 0 == ret ) {
		LPERROR("Zero bytes were sent.\r\n");
	} else if ( ret < len ){
		LPERROR("Unable to send the whole buffer. ret = %d\r\n", ret);
	} else {
		LPRINTF("Success sent data with len %d\r\n", len);
		for ( int i =0; i<len; i++ ){
			LPRINTF("0x%x\r\n", buf[i]);
		}
	}

	/* If something is sent with success to the agent,
	 * it means that the next expected paquet will be received later.
	 * Thus, the read_buffer_h can be emptied. */
//	LPRINTF("Data sent. Emptying the read buffer.\r\n");
//	if ( xMessageBufferReset(read_buffer_h) != pdTRUE )
//		LPERROR("Unable to empty the read_buffer_h after read.\r\n");


	//LPRINTF("Data sent. Re-enabling IRQ.\r\n");
	//metal_irq_restore_enable(flags);

	return ret;
}

/****************************************************************************/
/**
*
* This function should read data to the custom transport.
*
* WARRNING:
* This function works in a recursive way,
* calling itself until the len is zero.
*
* @param transport is a uxrCustomTransport structure
* @param buf
* @param len
* @param timeout  is unused as for now
* @param err is unused as for now
*
* @return	the number of Bytes read.
*
* @note		None.
*
*****************************************************************************/
size_t my_custom_transport_read(struct uxrCustomTransport* transport, uint8_t* buf, size_t len, int timeout, uint8_t* err)
{

	//LPRINTF("Trying to receive data of length %d from the read_buffer_h: 0x%x.\r\n", len, read_buffer_h);

	const TickType_t xBlockTime = pdMS_TO_TICKS( 1000 );
	size_t received_bytes = 0;

	if ( 0 >= len ){
		LPERROR("trying to read a length < 1\r\n");
	} else if ( xMessageBufferIsEmpty(read_buffer_h) == pdTRUE ) {
		//LPRINTF("message buffer is empty, nothing to be read\r\n");
		return 0;
	} else {
		//LPRINTF("micro-ROS read function. Trying to receive %d bytes.\r\n", len);

		LPRINTF("Wanna %d bytes. read_buffer_h now contains %d bytes\r\n", len,
				(xBufferSizeBytes - xMessageBufferSpacesAvailable(read_buffer_h)));

		received_bytes = xStreamBufferReceive( read_buffer_h,
											buf,
											len,
											xBlockTime );
		LPRINTF("%d bytes were read, from the %d requested. Read data are\r\n", received_bytes, len);
		for ( int i =0; i<received_bytes; i++ ) LPRINTF("0x%x\r\n", buf[i]);

		LPRINTF("Reset the flag asking to flush the buffer at the next cb\r\n");
		fresh_agent_packets_flag = 0;

		return received_bytes;
	}

//	LPERROR("rpmsg_read failed\r\n");
	return 0;
}
#endif //RMW_UXRCE_TRANSPORT_CUSTOM

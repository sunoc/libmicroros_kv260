################################################################################
# Automatically-generated file. Do not edit!
################################################################################

# Add inputs and outputs from these tool invocations to the build variables 
LD_SRCS += \
../src/lscript.ld 

C_SRCS += \
../src/allocators.c \
../src/clock.c \
../src/custom_transports.c \
../src/helper.c \
../src/memory_manager.c \
../src/microros_task.c \
../src/platform_info.c \
../src/rpmsg-echo.c \
../src/rpmsg_task.c \
../src/rsc_table.c \
../src/zynqmp_r5_a53_rproc.c 

OBJS += \
./src/allocators.o \
./src/clock.o \
./src/custom_transports.o \
./src/helper.o \
./src/memory_manager.o \
./src/microros_task.o \
./src/platform_info.o \
./src/rpmsg-echo.o \
./src/rpmsg_task.o \
./src/rsc_table.o \
./src/zynqmp_r5_a53_rproc.o 

C_DEPS += \
./src/allocators.d \
./src/clock.d \
./src/custom_transports.d \
./src/helper.d \
./src/memory_manager.d \
./src/microros_task.d \
./src/platform_info.d \
./src/rpmsg-echo.d \
./src/rpmsg_task.d \
./src/rsc_table.d \
./src/zynqmp_r5_a53_rproc.d 


# Each subdirectory must supply rules for building sources it contributes
src/%.o: ../src/%.c
	@echo 'Building file: $<'
	@echo 'Invoking: ARM R5 gcc compiler'
	armr5-none-eabi-gcc -DARMR5 -Wall -O0 -g3 -I/home/sunoc/workspace/microros_lib/include -I/home/sunoc/workspace/microros_lib/include/actionlib_msgs -I/home/sunoc/workspace/microros_lib/include/service_msgs -I/home/sunoc/workspace/microros_lib/include/rosidl_dynamic_typesupport -I/home/sunoc/workspace/microros_lib/include/tracetools -I/home/sunoc/workspace/microros_lib/include/action_msgs -I/home/sunoc/workspace/microros_lib/include/rcl_action -I/home/sunoc/workspace/microros_lib/include/rosidl_runtime_c -I/home/sunoc/workspace/microros_lib/include/rosidl_typesupport_interface -I/home/sunoc/workspace/microros_lib/include/std_msgs -I/home/sunoc/workspace/microros_lib/include/rcl -I/home/sunoc/workspace/microros_lib/include/rcutils -I/home/sunoc/workspace/microros_lib/include/rmw -I/home/sunoc/workspace/microros_lib/include/builtin_interfaces -I/home/sunoc/workspace/microros_lib/include/unique_identifier_msgs -c -fmessage-length=0 -MT"$@" -mcpu=cortex-r5  -mfloat-abi=hard  -mfpu=vfpv3-d16 -I/home/sunoc/workspace/k26_base_starter_kit/export/k26_base_starter_kit/sw/k26_base_starter_kit/freertos10_xilinx_psu_cortexr5_0/bspinclude/include -MMD -MP -MF"$(@:%.o=%.d)" -MT"$(@)" -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '


